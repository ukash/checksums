using Xunit;

namespace CheckSums
{
	public class CRCTest
	{
		[Theory]
		[InlineData("11010011101110", "1011", 3, "010")]
		public void TestCalculating(string bits, string divider, int n, string result)
		{
			var crc = new CRC();

			Assert.Equal(result, crc.calculate(bits, divider, n));
		}

		[Theory]
		[InlineData("11010011101110", "1011", 3, "010", "000")]
		public void TestVerifying(string bits, string divider, int n, string crc, string result)
		{
			var crcI = new CRC();

			Assert.Equal(result, crcI.calculate(bits, divider, n, crc));
		}

		[Theory]
		[InlineData("Slawek to miszcz.", "1011", 6, "001100")]
		[InlineData("Slawek to miszcz, ze hej!", "1011", 3, "000")]
		public void TestCalculatingString(string bits, string divider, int n, string result)
		{
			var crc = new CRC();

			Assert.Equal(result, crc.calculateString(bits, divider, n));
		}

		[Theory]
		[InlineData("Slawek to miszcz.", "1011", 6, "001100", "000000")]
		[InlineData("Slawek to miszcz, ze hej!", "1011", 3, "000", "000")]
		public void TestVerifyingString(string bits, string divider, int n, string crc, string result)
		{
			var crcI = new CRC();

			Assert.Equal(result, crcI.calculateString(bits, divider, n, crc));
		}
	}
}