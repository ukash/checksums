using System;
using System.Text;

namespace CheckSums
{
	public class CRC
	{
		public string calculate(string bits, string divider, int n, string crc = null)
		{
			if (divider.Length > bits.Length)
				throw new ArgumentOutOfRangeException("Divider cannot be longer than bits string.", divider);

			var bitsCopy = new StringBuilder(string.Concat(bits, generateOffset(n, crc)));

			for (int i = 0; i < bitsCopy.Length - divider.Length; i++)
				if (bitsCopy[i] == '1')
					for (int j = 0; j < divider.Length; j++)
						bitsCopy[i + j] = XorString(bitsCopy[i + j], divider[j]);

			return bitsCopy.Remove(0, bits.Length).ToString();
		}

		private string generateOffset(int n, string crc = null)
			=> crc ?? new String('0', n);

		private char XorString(char a, char b)
			=> int.Parse(a.ToString()) + int.Parse(b.ToString()) == 1 ? '1' : '0';

		public static string StringToBinary(string data)
		{
			StringBuilder sb = new StringBuilder();

			foreach (char c in data.ToCharArray())
			{
				sb.Append(Convert.ToString(c, 2).PadLeft(8, '0'));
			}
			return sb.ToString();
		}
		public string calculateString(string bits, string divider, int n, string crc = null) => calculate(StringToBinary(bits), divider, n, crc);
	}

}